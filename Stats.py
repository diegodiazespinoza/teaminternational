from functools import reduce


class Stats:
    """
    Receive a list and calculate the structure that will
    allow answering stats about it. See calculate() method.

   """

    def __init__(self, bucket) -> None:
        super().__init__()
        self.bucket = bucket
        self.counter = [0] * len(self.bucket)
        self.calculate()

    """
        Iterate over the list (bucket) just once (remember the list has **fixed** size  
        DataCapture.C; in the challenge this number is 1.000).
        
        For every index J in the list the sum of each value of the list with indexes less 
        than J and greater than J is calculated using sum function and stored as a tuple 
        in a new list named counter, where every index represent a number between 0 and DataCapture.C
        ** Note that the list has fixed length 1.000 and
        we do just one iteration and for each iteration the function sum will do
        no more than 1.000 iterations; this seems to be a nested loop hidden in a
        sum **but** note that the overall iterations **doesn't** depends on the
        length of the total numbers in the original bucket because the **fixed** 
        length is set a priori at the beginning, so time complexity is bounded
        by DataCapture.C hence it is at most O(n)   
    """

    def calculate(self):
        self.counter[0] = (0, sum(self.bucket))
        for j in range(1, len(self.bucket)):
            self.counter[j] = (sum(self.bucket[:j]), sum(self.bucket[j + 1:]))
        return self.counter

    """
        Every item in *counter* has a tuple representing total numbers
        before the index and total numbers after the index
        Args:
            num: a positive integer between [0, DataCapture.C]
    
        Returns:
            Total numbers less than num
        
    """

    def less(self, num=0) -> []:
        if num < 0 or num > len(self.bucket)+1:
            raise IndexError()
        return self.counter[num][0]

    """
        Every item in *counter* has a tuple representing total numbers
        before the index and total numbers after the index

        Args:
            num: a positive integer between [0, DataCapture.C]

        Returns:
            Total numbers greater than num

        """

    def greater(self, num=0) -> []:
        if num < 0 or num > len(self.bucket)+1:
            raise IndexError()
        return self.counter[num][1]

    """
                
        Limits has no order (so limit1>=limit2 or limit1<=limit2 is the same) 

        Args:
            limit_1: a positive integer between [0, DataCapture.C]
            limit_2: a positive integer between [0, DataCapture.C]

        Returns:
            Total numbers between min(limit_1, limit_2) and max(limit_1, limit_2)

    """

    def between(self, limit_1=0, limit_2=0) -> []:
        _min = min(limit_1, limit_2)
        _max = max(limit_1, limit_2)
        if _min < 0 or _min > len(self.bucket)+1:
            raise IndexError()
        if _max < 0 or _max > len(self.bucket)+1:
            raise IndexError()
        start = max(0, _min-1)
        end = _max + 1
        return self.greater(start) - self.greater(end)
