from Stats import Stats


class DataCapture:
    """
        DataCapture allows to keep any amount of numbers
        but all of them between 1 and DataCapture.C
        By now C is fixed at C = 1000. The most importante
        here is that C is fixed and doesn´t change in runtime,
        but the amoun of numbers that can be kept is not fixed

    """

    """ In the chanllenge talks about numbers less than 1000 """
    C = 10 + 1

    """
        
        Args:
            No args
        
        Create a bucket (list) to receive any amount of numbers
        between 0 and DataCapture.C
        
    """

    def __init__(self) -> None:
        super().__init__()
        self.bucket = [0] * DataCapture.C

    """
    Allows to add numbers in the bucket. The method
    increment by 1 the position represented by the number
    that wants to be added. As an example, if we want to add
    number 3, so the value at index position 3 in the bucket
    will be incremented by 1.
    As the only action is access an item in the list, so
    this methos ensure O(1)

    Args:
        small_int: a positive integer [0, DataCapture.C]

    Returns:
        None

    Raises:
        KeyError: Raises an exception if small_int is out of bounds
        defined by [0, DataCapture.C].
        
    """
    def add(self, small_int):
        if 0 > small_int > DataCapture.C:
            raise Exception("Only positive integers between 0 and {} are allowed".format(str(DataCapture.C)))
        self.bucket[small_int] += 1

    """
    Create an object Stats. For the object creation
    it passes the bucket.

    Returns:
        A Stats object (see class Stats.py)
        

    """
    def build_stats(self) -> Stats:
        stats = Stats(self.bucket)
        return stats

