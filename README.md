# README #

This code was made for a test challenge for Team International.
Conditions, restrictions and the problem itself is inside this repo 
in the file TECH_CHALLENGE_PYTHON_SENIOR.pdf.

The solution considers two classes separated in two files:

DataCapture.py -> contains DataCapture class
Stats.py -> contains Stats class

Unit test using unittests are in Test.py

