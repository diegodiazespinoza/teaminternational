import unittest
from DataCapture import DataCapture


class TechChallengeTest(unittest.TestCase):
    data_capture = None
    stats = None

    def setUp(self):
        self.data_capture = DataCapture()
        self.data_capture.add(3)
        self.data_capture.add(9)
        self.data_capture.add(3)
        self.data_capture.add(4)
        self.data_capture.add(6)
        self.stats = self.data_capture.build_stats()

    def test_0_add(self):
        self.stats = self.data_capture.build_stats()

    def test_1_less_stats(self):
        assert self.stats.less(4) == 2

    def test_2_between_stats(self):
        assert self.stats.between(3, 6) == 4

    def test_3_greater_stats(self):
        assert self.stats.greater(4) == 2

    def test_4_less_stats_(self):
        assert self.stats.less(0) == 0

    def test_6_between_stats_(self):
        assert self.stats.between(0,9) == 5

    def test_7_greater_stats_(self):
        with self.assertRaises(IndexError):
            self.stats.greater(100)

    def test_8_less_stats_(self):
        with self.assertRaises(IndexError):
            self.stats.less(-1)

    def test_9_between_stats_(self):
        assert self.stats.between(0,9) == 5

    def test_10_between_stats_(self):
        with self.assertRaises(IndexError):
            self.stats.between(-1,9)

    def test_11_between_stats_(self):
        with self.assertRaises(IndexError):
            self.stats.between(-1,-9)

    def test_12_greater_stats_(self):
        assert self.stats.greater(0) == 5

    def test_13_greater_stats_(self):
        with self.assertRaises(IndexError):
            self.stats.greater(-4)

if __name__ == "__main__":
    unittest.main()
